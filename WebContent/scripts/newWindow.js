/**
 * Open a new window with the size passed
 */

function newWindow(pagePath, width, height){
	newWindow = window.open(pagePath, 'newWindow', 'width=' + width + ', height=' + height);
	return true;
}

function newPostWindow(width, height, handlerPath){
	newWindow = window.open(null, 'newWindow', 'width=' + width + ', height=' + height);
	newWindow.document.write('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">			<html>			<head>			<meta charset="utf-8">			<meta http-equiv="X-UA-Compatible" content="IE=edge">			<meta name="viewport" content="width=device-width, initial-scale=1">			<meta name="description" content="">			<meta name="author" content="">			<link rel="shortcut icon" href="/images/LOGOfavicon.ico"				type="image/x-icon">			<link rel="icon" href="/images/LOGOfavicon.ico" type="image/x-icon">			<!-- Bootstrap core CSS -->			<link href="css/bootstrap.min.css" rel="stylesheet">			<!-- Custom styles for this template -->			<link href="css/custom/generalStyle.css" rel="stylesheet">			<title>Inserir novo post</title>			</head>			<body>				<form method="POST" name="newPost_form"	action="'+ handlerPath + '">					<div class="form-group">						<label for="exampleInputPassword1">Titulo</label> <input							name="post_title" type="text" class="form-control"							placeholder="Titulo">					</div>					<div class="form-group">						<label for="exampleInputPassword1">Texto</label>						<textarea class="form-control" rows="5" name="post_body"							placeholder="Digite o texto do post..."></textarea>					</div>					<button type="submit" class="btn btn-default">Postar</button>				</form>		<!-- Bootstrap core JavaScript				    ================================================== -->				<!-- Placed at the end of the document so the pages load faster -->				<script					src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>				<script src="../js/bootstrap.min.js"></script>			</body>			</html>');
	return true;
}