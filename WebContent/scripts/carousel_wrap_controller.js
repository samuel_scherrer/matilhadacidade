/**
 * Controller for component carousel wrap
 */

/** 
*Variáveis de ajuste do carousel
**/
var DELTA_SLIDE = 1;
var CAROUSEL_WRAP_ID = 'carousel_wrapper';
var TIME_INTERVAL = 50;
var IMAGE_WIDTH = 200;
var NUM_REP = 10;
var LEFT_OFFSET = 0;
var RIGHT_OFFSET = 0;

/**
 * Variáveis globais
 */
var currWinWidth = window.innerWidth;
var winWidth = window.innerWidth - RIGHT_OFFSET - LEFT_OFFSET;

/**
*Metodo inicial, seta o intervalo de tempo com que a função update será chamada
**/
function init() {
	//Cria as tags <img> e injeta no html
	loadCarouselImgs()
	
	setInterval(update, TIME_INTERVAL);
}

/**
 * Cria as tags img e injeta no HTML
 * @param numImgs
 */
function loadCarouselImgs(){
	//Calculamos o tamanho da janela
	winWidth = window.innerWidth - RIGHT_OFFSET - LEFT_OFFSET;
	//A quantidade de imagens é a rendondada para cima e acresido de 1. Isso se deve a situação em que por exemplo
	//temos 5.5 imgs. Logo precisamos de 6 + 1 imgs para fechar o circulo
	var numOfImgs = Math.ceil(winWidth / IMAGE_WIDTH) + 1;
	
	//Num da imagen a ser usada
	var imgNum = 1;
	var pos = 0;
	var str = '';
	var inner = "";
	for(var imgID = 0; imgID < numOfImgs; imgID++){
		pos = LEFT_OFFSET + (imgID * IMAGE_WIDTH);
		str = '<img class="wrap_img" id="img'+imgID+'" src="../images/carousel_wrap/'+imgNum+'.jpg" onmouseover="toColor('+
		'\'img'+imgID+'\');" onmouseout="toGray(\'img'+imgID+'\');" style="transform: translate3d('+pos+'px, 0px, 0px); -webkit-filter:grayscale(100%); filter:grayscale(100%)">'
		
		inner = inner + str;
		
		imgNum++;
		
		//O num_rep indica quantas imagens temos disponíveis assim ao atingir o máximo, começar a repetir
		if(imgNum > NUM_REP){
			imgNum = 1;
		}
			
	}
	
	document.getElementById(CAROUSEL_WRAP_ID).innerHTML = inner;

}

/**
*Recupera todos os nos filhos da DIV carousel_wrap. Observe que se entre a declaração de cada filho no html existir entre ('\n')
*esse vai ser recuperado, por esse motivo devemos checar o nodeType. Se for 3 deve ser descartado
**/
function getNodes(){
	var nodes = document.getElementById(CAROUSEL_WRAP_ID).childNodes;
	var cleanNodes = [];

	for(var i = 0; i < nodes.length; i++){
		//O numero 3 indica ser nodeType para o \n
		if(nodes[i].nodeType != 3){
			cleanNodes.push(nodes[i]);
		}
	}
	return cleanNodes;
}

/**
 * Remove todos os nós do CAROUSEL_WRAP
 */
function removerNodes(){
	var myNode = document.getElementById(CAROUSEL_WRAP_ID);
	//removemos toods
	while (myNode.firstChild) {
			myNode.removeChild(myNode.firstChild);
	}	
}

/**
*Chamada a cada intervalo de tempo. Realiza o update dos nós
**/
function update(){
	//Verifica se o tamanho da janela mudou
	if(currWinWidth != window.innerWidth){
		currWinWidth = window.innerWidth;
		//remover imgs e refazer o carousel
		removerNodes();
		//carregar novamente as imagens
		loadCarouselImgs();
	}
	
	//Pegamos os nodes dentro da div carousel_wrap
	var nodes = getNodes();
	for(var i = 0; i < nodes.length; i++){
		//Para cada node realizamos o slide
		var borderTouched = slide(nodes[i], i);

		//Se o nó é o último ele pode ter tocado a borda da tela, caso ele tocou devemos reordenar os nos tornando o último o primeiro
		if(borderTouched){
			var newNodesOrder = makeLastFirst(nodes);
			nodes = getNodes();

			//Limpamos o carousel
			removerNodes();
			//Adicionamos os nós novamente porém agora na ordem desejada
			for(var j = 0; j < newNodesOrder.length; j++){
				document.getElementById(CAROUSEL_WRAP_ID).appendChild(newNodesOrder[j]);
			}
		}
	}
}

/**
*Método que realiza o slide de cada nó.
*Após realizar o slide verifica se o nó tocou a borda da tela, em caso positivo retorno true
**/
function slide(imgToSlide, imgNodeIndex){
	var borderTouched = false;
	var transform = imgToSlide.style.transform;
	var pxPos = transform.indexOf("px");
	var currPos = parseInt(transform.substring(transform.indexOf("(") + 1, pxPos));

	if(currPos > winWidth){
		currPos = -IMAGE_WIDTH + LEFT_OFFSET;//Arruamr para voltar exatamente o tamanho da foto
		borderTouched = true;
	}

	var nextPos = currPos + DELTA_SLIDE;
	imgToSlide.style.transform = "translate3d(" + nextPos + "px, 0px, 0px)";
	return borderTouched;
}

/**
*Inverte o ordem dos nós. Recebe um array com nós e retorno o mesmo array porém onde o último nó passou a ser o primeiro
**/
function makeLastFirst(nodes){
	var newNodes = nodes;
	var last = nodes[nodes.length - 1];
	for(var i = nodes.length - 2; i >= 0; i--){
		newNodes[i + 1] = nodes[i];
		if(i == 0){
			newNodes[i] = last;
		}
	}
	return newNodes;
}

/**
*Realiza a troca da imagem atual para a imagem passada pelo newImgName no elemento element
**/
function toColor(element){
	var transform = document.getElementById(element).style.transform;
	document.getElementById(element).setAttribute("style", "transform: "+transform+";");
}

/**
*Faz o mesmo que o toColor porém quebramos em dois só para os métodos terem nomes diferentes
**/
function toGray(element){
	var transform = document.getElementById(element).style.transform;
	document.getElementById(element).setAttribute("style", "transform: "+transform +"; filter: grayscale(100%); -webkit-filter: grayscale(100%);");
}


/**
*Recupera o nome do arquivo da imagem apartir do path do src completo
*O nome da image é o último componente da string depois do último '/'
**/
function getCurrImgName(srcPath){
	//Pegamos a posição da última ocorrencia de uma /
	var posBeforeName = srcPath.lastIndexOf("/");
	//Pegamos a substring da posicao anterio + 1 (primeira lentra do nome da image) até o fim da string
	var name = srcPath.substring(posBeforeName + 1, srcPath.length);
	return name;
}

//Start the script
init();

