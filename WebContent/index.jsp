<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ page import="blog.post.PostFragment" %>
<%@ page import="dao.postfragment.*"%>
<%@ page import="java.util.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<link rel="shortcut icon" href="/images/LOGOfavicon.ico"
	type="image/x-icon">
<link rel="icon" href="/images/LOGOfavicon.ico" type="image/x-icon">

<title>Matilha da Cidade</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/custom/navBar.css" rel="stylesheet">
<link href="css/custom/indexPage.css" rel="stylesheet">
<link href="css/custom/generalStyle.css" rel="stylesheet">
<link href="css/custom/footer.css" rel="stylesheet">

</head>

<body id="body">

	<!-- NavBar -->
	<jsp:include page="pages/modules/topNavBar.jsp">
		<jsp:param name="callerName" value="index" />
	</jsp:include>
	<!-- /.NavBar -->

	<!-- Carousel -->
	<div id="carousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#carousel" data-slide-to="0" class="active"></li>
			<li data-target="#carousel" data-slide-to="1"></li>
			<li data-target="#carousel" data-slide-to="2"></li>
			<li data-target="#carousel" data-slide-to="3"></li>
			<li data-target="#carousel" data-slide-to="4"></li>
		</ol>
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<img class="carouselImg" src="images/carousel/index1.jpg" alt="First slide">
				<div class="container">
					<div class="carousel-caption">
						<h1>Passeios orientados</h1>
						<p>A Matilha da Cidade oferece passeios orientados. Assim al�m
							de gastar energia no passeio, seu cachorro tamb�m faz um
							exerc�cio mental.</p>
					</div>
				</div>
			</div>
			<div class="item">
				<img class="carouselImg" src="images/carousel/index2.jpg" alt="Second slide">
				<div class="container">
					<div class="carousel-caption">
						<h1>Para todos os c�es</h1>
						<p>Independente do tamanho do seu c�o, passeios di�rios s�o
							indispens�veis para a sua sa�de e bem estar.</p>
					</div>
				</div>
			</div>
			<div class="item">
				<img class="carouselImg" src="images/carousel/index3.jpg" alt="Third slide">
				<div class="container">
					<div class="carousel-caption">
						<h1>Pet Sitter</h1>
						<p>Vai viajar e n�o pode levar seu amig�o? A Matilha da Cidade
							pode te ajudar</p>
					</div>
				</div>
			</div>
			<div class="item">
				<img class="carouselImg" src="images/carousel/index4.jpg" alt="Third slide">
				<div class="container">
					<div class="carousel-caption">
						<h1></h1>
						<p></p>
					</div>
				</div>
			</div>
			<div class="item">
				<img class="carouselImg" src="images/carousel/index5.jpg" alt="Third slide">
				<div class="container">
					<div class="carousel-caption">
						<h1></h1>
						<p></p>
					</div>
				</div>
			</div>
		</div>
		<a class="left carousel-control" href="#carousel" role="button"
			data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"
			aria-hidden="true"></span> <span class="sr-only">Previous</span>
		</a> <a class="right carousel-control" href="#carousel" role="button"
			data-slide="next"> <span
			class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
	<!-- /.carousel -->

	<!-- Pensamentos/Not�cias -->
	<div class="container marketing">
		<div class="row">
			<!--  Pensamentos, usando imagem de fundo -->
			<div id="thinking" class="col-lg-4">
			</div>
			
			<!--  Not�cias da Matilha -->
			<div id="blog" class="col-lg-4">
				<h2>Not�cias da Matilha:</h2>

				<ul id="scrollable-area" class="scrollable-menu">

					<%
						PostFragmentDAO postFragDAO = new PostFragmentDAO();

						List<PostFragment> list = postFragDAO.getAll();
						
						for(int i = 0; i < list.size(); i++){
							PostFragment postFrag = list.get(i);
							
					%>
							<div id="<%=postFrag.getID() %>" href="#<%=postFrag.getID() %>" class="list-group-item">
								<h4 class="list-group-item-heading"><%=postFrag.getTitle() %></h4>
								<p class="list-group-item-text"><%=postFrag.getMessage() %></p>
							</div>
					
					<%
							
						}
					
					%>
					
				</ul>
			</div>
		</div>
		<!-- /.row -->
	</div>
	<!-- /.Pensamentos/Not�cias -->

	<!-- Site footer -->
	<jsp:include page="pages/modules/footer.jsp">
		<jsp:param name="callerName" value="index" />
	</jsp:include>
	<!-- /footer -->

	<!-- Bootstrap core JavaScript
	    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

</body>
</html>