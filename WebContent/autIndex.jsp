<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Matilha da Cidade</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/custom/navBar.css" rel="stylesheet">
<link href="css/custom/autIndex.css" rel="stylesheet">
<link href="css/custom/generalStyle.css" rel="stylesheet">
<link href="css/custom/footer.css" rel="stylesheet">

</head>

<body>

	<!-- NavBar -->
	<div class="navbar-wrapper">
		<div class="container">
			<!-- Fixed navbar -->
			<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.jsp"><img id="MainBarLogo"
						src="images/logo.png" /></a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">

					<ul class="nav navbar-nav navbar-right">
						<li><a class="linkOtherLogos"
							href="https://www.facebook.com/MatilhaDaCidade" target="_blank"><img
								class="imageOtherLogos" src="images/facebook.png" width="30px"
								height="30px"></a></li>
						<li><a class="linkOtherLogos"
							href="https://www.facebook.com/MatilhaDaCidade" target="_blank"><img
								class="imageOtherLogos" src="images/Instagram.png" width="30px"
								height="30px"></a></li>
					</ul>
				</div>
			</div>
			</nav>
			<br> <br>
		</div>
	</div>
	<!-- /.NavBar -->

	<div id="content-container" class="container">

		<!-- Main component for a primary marketing message or call to action -->
		<div class="jumbotron">
			<div id="mainText">
				<h1 id="h1">Site em manuten��o</h1>
				<p class="p">Entre em contato pelo email:</p>
				<p class="p">matilhadacidade@gmail.com</p>
				<p class="p">Ou pelos telefones:</p>
				<p class="p"> (11)96103-3424 (D�bora)</p>
				<p class="p"> (11)96709-1672 (Marina)</p>
			</div>
		</div>
	</div>
	<!-- /container -->

	<!-- Site footer -->
	<jsp:include page="pages/modules/footer.jsp">
		<jsp:param name="callerName" value="index" />
	</jsp:include>
	<!-- /footer -->


	<!-- Bootstrap core JavaScript
	    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>

</body>
</html>