<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ page import="org.gjt.mm.mysql.*"%>
<%@ page import="user.User"%>
<%@ page import="dao.post.PostDAO"%>
<%@ page import="blog.post.Post"%>
<%@ page import="blog.comment.Comment"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!--  Ativa o modo para compatibilidade com mobile -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Matilha da Cidade</title>

<!-- Scripts -->
<script src="../scripts/newWindow.js"></script>

<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="../css/custom/navBar.css" rel="stylesheet">
<link href="../css/custom/blogPage.css" rel="stylesheet">
<link href="../css/custom/generalStyle.css" rel="stylesheet">
<link href="../css/custom/footer.css" rel="stylesheet">



</head>
<body id="body">

	<!-- NavBar -->
	<jsp:include page="modules/topNavBar.jsp">
		<jsp:param name="callerName" value="blog" />
	</jsp:include>
	<!-- /.NavBar -->


	<div id="content-container" class="container">

		<div id="headerImage" class="jumbotron">
			<h1 id="headerText">Blog da Matilha</h1>
		</div>
		<br>

		<!--  BOTAO PARA NOVOS POSTS -->
		<%
		if(request.getSession().getAttribute("loggedUser") != null) {
			User loggedUser = (User)request.getSession().getAttribute("loggedUser");
			if(loggedUser.isAdmin()){ %>
		<form
			onSubmit="JavaScript:newPostWindow('400', '500', 'modules/new_post_handler.jsp')">
			<button type="submit" class="btn btn-default">Postar</button>
		</form>
		<!-- <form onSubmit="JavaScript:onClick()')">
					<button type="submit" class="btn btn-default">Remove</button>
				</form> -->
			<%} %>




		<%}
		PostDAO postDAO = new PostDAO();
		List<Post> postList = postDAO.getAll();
		boolean hasComment;
		
		for(int i = 0; i < postList.size(); i++){
			Post post = postList.get(i);
			if(post.getComments().size() > 0)
				hasComment = true;
			else
				hasComment = false;
		%>
		
			<!-- Post -->
			<jsp:include page="modules/post.jsp">
				<jsp:param name="post_id" value="<%=post.getID()%>" />
				<jsp:param name="post_author" value="<%=post.getAuthor()%>" />
				<jsp:param name="post_title" value="<%=post.getTitle()%>" />
				<jsp:param name="post_text" value="<%=post.getMessage()%>" />
				<jsp:param name="post_date" value="<%=post.getDate()%>" />
				<jsp:param name="post_likes" value="<%=post.getLikes()%>"/>
				<jsp:param name="post_hasComments" value="<%=hasComment %>"/>
			</jsp:include>
			<!-- /.Post -->	
			
			<%	List<Comment> commentList = post.getComments();
				//Verifica se temos coment�rios neste post
				if(hasComment){
			%>
					<span id="postid-<%=post.getID()%>" style="display: none">
			<%
					for(int j = 0; j < commentList.size(); j++){
						Comment comment = commentList.get(j);
			%>	
						<!-- Comment -->
						<jsp:include page="modules/comment.jsp">
							<jsp:param name="comment_id" value="<%=comment.getID() %>" />
							<jsp:param name="comment_author" value="<%=comment.getAuthor()%>" />
							<jsp:param name="comment_text" value="<%=comment.getMessage()%>" />
							<jsp:param name="comment_date" value="<%=comment.getData()%>" />
						</jsp:include>
						<!-- /.Comment -->
						
						<!-- Se � o �ltimo coment�rio colocar abaixo dele o campo para comentar -->
						<% if(j + 1 > commentList.size() - 1){ %>
							<!-- Write Comment -->
							<jsp:include page="modules/writeComment.jsp">
								<jsp:param name="post_id" value="<%=post.getID()%>" />
								<jsp:param name="default_display" value="block" />
							</jsp:include>
							<!-- /.Write Comment -->
						<%}%>

			<%
					}
			%>
					</span>
			<%	
				}
			%>
			<!-- espa�o entre post -->
			<br>
			<%
		}	
		%>

	</div>
	<!-- /container -->


	<!-- Site footer -->
	<jsp:include page="modules/footer.jsp">
		<jsp:param name="callerName" value="blog" />
	</jsp:include>
	<!-- /footer -->


	<!-- Bootstrap core JavaScript
	    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>

</body>
</html>