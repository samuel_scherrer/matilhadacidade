<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Matilha da Cidade</title>

<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="../css/custom/navBar.css" rel="stylesheet">
<link href="../css/custom/servicesPage.css" rel="stylesheet">
<link href="../css/custom/generalStyle.css" rel="stylesheet">
<link href="../css/custom/footer.css" rel="stylesheet">

<!-- Esse script � respons�vel adicionar um listener para o media queries do css.
	Assim quando a janela estiver em 1199 ou menos iremos setar primeiro o texto e depois a imagem para a div com id invertible.
	Quando a janela passar para 1200 invertemos e setamos a imagem depois o texto para a div, pois agora queremos um do lado do outro -->
<script type="text/javascript">
function init(){
	var mql = window.matchMedia("(max-width: 1199px)");
	mql.addListener(handleMediaChange);
	handleMediaChange(mql);
}

var handleMediaChange = function(mediaQueryList){
	var textFirst = '<div class="col-lg-4"><h2 id="img-sub-2">Pet Sitter</h2><p>Via viajar e n�o quer deixar seu amig�o em hotelzinho?</p>'
		+ '<p>A Matilha da Cidade cuida do seu c�o na sua casa, evitando assim o estresse de estar sem o dono e longe da sua casa.</p></div><div class="col-lg-4">'
		+ '<img id="imgPetSitter" class="services-img" src="../images/servicesPetSitter.jpg"></div></div>';		
		
	var imgFirst = '<div class="col-lg-4"><img id="imgPetSitter" class="services-img" src="../images/servicesPetSitter.jpg"></div>'
	 + '<div class="col-lg-4"><h2 id="img-sub-2">Pet Sitter</h2><p>Via viajar e n�o quer deixar seu amig�o em hotelzinho?</p>'
	 + '<p>A Matilha da Cidade cuida do seu c�o na sua casa, evitando assim o estresse de estar sem o dono e longe da sua casa.</p></div></div>';
	
	if(mediaQueryList.matches){
		document.getElementById('invertible-row').innerHTML = textFirst; 
	}
	else {
		document.getElementById('invertible-row').innerHTML = imgFirst;
	}
}

</script>

</head>
<body id="body" onload="init();">

	<!-- NavBar -->
	<jsp:include page="modules/topNavBar.jsp">
		<jsp:param name="callerName" value="services" />
	</jsp:include>
	<!-- /.NavBar -->

	<div id="content-container" class="container">

		<div id="headerImage" class="jumbotron">
			<h1 id="headerText">Servi�os</h1>
		</div>

		<div class="jumbotron">
			<p align="justify">A Matilha da Cidade oferece passeios orientados onde seu c�o
				gasta energia de forma equilibrada, aprendendo a seguir o passeador
				como l�der.</p>
			<p align="justify">Podemos pegar seu c�o para o passeio enquanto voc� est�
				trabalhando, evitando assim que ele fique muito tempo sozinho.</p>
		</div>
		<!-- /container -->

		<!-- Example row of columns -->
		<div class="row">
			<div class="col-lg-4">
				<h2 id="img-sub-1">Passeios orientados (Dog Walker)</h2>
				<p align="justify">A Matilha da Cidade oferece passeios orientados onde seu c�o
					gasta energia de forma equilibrada, aprendendo a seguir o passeador
					como l�der.</p>
				<p align="justify">Podemos pegar seu c�o para o passeio enquanto voc� est�
					trabalhando, evitando assim que ele fique muito tempo sozinho.</p>
			</div>
			<div class="col-lg-4">
				<img id="imgDogWalker" class="services-img"
					src="../images/servicesDogWalking.jpg">
			</div>

		</div>
		<br>

		<div id="invertible-row" class="row">
			<div class="col-lg-4">
				<img id="imgPetSitter" class="services-img"
					src="../images/servicesPetSitter.jpg">
			</div>
			<div class="col-lg-4">
				<h2 id="img-sub-2">Pet Sitter</h2>
				<p align="justify">Via viajar e n�o quer deixar seu amig�o em hotelzinho?</p>
				<p align="justify">A Matilha da Cidade cuida do seu c�o na sua casa, evitando
					assim o estresse de estar sem o dono e longe da sua casa.</p>
			</div>
		</div>
	</div>

	<!-- Site footer -->
	<jsp:include page="modules/footer.jsp">
		<jsp:param name="callerName" value="services" />
	</jsp:include>
	<!-- /footer -->


	<!-- Bootstrap core JavaScript
	    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>

</body>
</html>