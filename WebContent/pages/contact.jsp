<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="math.utils.RandomGenerator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Matilha da Cidade</title>

<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="../css/custom/navBar.css" rel="stylesheet">
<link href="../css/custom/contactPage.css" rel="stylesheet">
<link href="../css/custom/generalStyle.css" rel="stylesheet">
<link href="../css/custom/footer.css" rel="stylesheet">


</head>

<body id="body">

	<!-- NavBar -->
	<jsp:include page="modules/topNavBar.jsp">
		<jsp:param name="callerName" value="contact" />
	</jsp:include>
	<!-- /.NavBar -->

	<div id="content-container" class="container">

		<div id="headerImage" class="jumbotron">
			<h1 id="headerText">Contatos</h1>
		</div>

		<!-- Main component for a primary marketing message or call to action -->
		<div class="jumbotron">
			<p align="justify">Ficou interessado nos nossos servi�os ou caso queira nos
				conhecer melhor, n�o hesite em nos contactar.</p>
			<p align="justify">Atendendo pelos numeros 91234-1234 (FULANO) ou 91234-1234
				(CICLANO)</p>
			<p align="justify">Ou ainda, � possivel entrar em contato enviando-nos uma mensagem:</p>

			<!-- Alert sobre envio da mensagem -->
			<%
				if (session.getAttribute("messageStatus") == null) {
				} else if (session.getAttribute("messageStatus").equals("SENT")) {
			%>
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				Mensagem enviada com sucesso!
			</div>
			<%
				session.setAttribute("messageStatus", null);
				} else {
			%>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				Erro ao enviar a mensagem:
				<%=session.getAttribute("errorString")%>
			</div>
			<%
				session.setAttribute("messageStatus", null);
				}
			%>
			<!-- /Alert -->

		</div>
		<div class="row">
			<div id="form_col" class="col-lg-4">
				<!-- Form -->
				<form method="POST" name="contact_form"
					action="modules/contact_form_handler.jsp">
					<div class="form-group">
						<input name="sender_email" type="email" class="form-control"
							placeholder="Digite seu email">
					</div>
					<div class="form-group">
						<input name="sender_name" type="text" class="form-control"
							placeholder="Seu nome">
					</div>
					<div class="form-group">
						<input name="email_subject" type="text" class="form-control"
							placeholder="Assunto">
					</div>
					<div class="form-group">
						<textarea class="form-control" rows="3" name="email_body"
							placeholder="Digite sua mensagem..."></textarea>
					</div>
					<% 
						String robotCheckQuestion = "Quanto � ";
						int rand1 = RandomGenerator.getRobotCheckNum();
						request.setAttribute("robotRand1", rand1);
						int rand2 = RandomGenerator.getRobotCheckNum();
						request.setAttribute("robotRand2", rand2);
						robotCheckQuestion = robotCheckQuestion + rand1 + " + " + rand2 + "?";
					%>
					<div class="form-group">
						<input name="robot_check" type="text" class="form-control"
							placeholder="<%=robotCheckQuestion %>">
					</div>
					<input id="boneButton" type="image" src="../images/buttons/boneButton.jpg" alt="Enviar" >
				</form>
				<!-- /Form -->
			</div>
			<div id="petImg_col" class="col-lg-4">
				<img id="petImg" src="../images/mascoteAnotando.jpg">
			</div>
		</div>
	</div>
	<!-- /container -->



	<!-- Site footer -->
	<jsp:include page="modules/footer.jsp">
		<jsp:param name="callerName" value="contact" />
	</jsp:include>
	<!-- /footer -->

	<!-- Bootstrap core JavaScript
	    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>

</body>
</html>