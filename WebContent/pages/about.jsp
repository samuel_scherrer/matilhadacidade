<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Matilha da Cidade</title>

<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="../css/custom/navBar.css" rel="stylesheet">
<link href="../css/custom/aboutPage.css" rel="stylesheet">
<link href="../css/custom/generalStyle.css" rel="stylesheet">
<link href="../css/custom/footer.css" rel="stylesheet">

</head>

<body id="body">


	<!-- NavBar -->
	<jsp:include page="modules/topNavBar.jsp">
		<jsp:param name="callerName" value="about" />
	</jsp:include>
	<!-- /.NavBar -->

	<div id="content-container" class="container">

		<div id="headerImage" class="jumbotron">
			<h1 id="headerText">Quem somos</h1>
		</div>

		<!-- Main component for a primary marketing message or call to action -->
		<div class="jumbotron">
			<p align="justify">Na correria do dia a dia nada melhor do que chegar em casa e
				ter seu melhor amigo te esperando para te dar muito amor e carinho,
				super feliz de te ver e com muita energia para brincar</p>
			<p align="justify">Mas depois de um dia de trabalho, na correria de S�o Paulo nem
				sempre os donos conseguem ter tempo para levar seu amig�o para
				passear e assim ajuda-lo a viver com sa�de e equil�brio e gastar
				energia.</p>
			<p align="justify">Assim, a Matilha da Cidade surgiu do conhecmento da
				import�ncia dos passeios para os c�es e pelo desejo desejo de ajudar
				esses donos que n�o querem abrir m�o da alegria que � a companhia de
				um c�o e que mesmo sem tempo se preocupam com o bem estar do seu
				melhor amigo!</p>
		</div>
	</div>
	<!-- /container -->

	<div class="container marketing">
		<h2>Nossa Equipe:</h2>

		<div class="row">
			<div class="col-lg-4">
				<img class="img-circle" src="../images/perfilDebora.png"
					alt="Foto D�bora">
				<h2 class="img-circle-sub">D�bora</h2>
				<p align="justify">D�bora � bi�loga e educadora ambiental. Depois de passar
					alguns anos dando aulas de biologia, resolveu deixar tudo para
					trabalhar com c�es, j� que sempre teve paix�o por eles. Fez o curso
					de Passeador de c�es da C�o Ativo e hoje oferece passeios
					orientados e outros cuidados para os cachorros da Matilha da
					Cidade.</p>
			</div>
			<div class="col-lg-4">
				<img class="img-rectangular" src="../images/dogSitting.png"
					alt="Dog">
			</div>
			<div class="col-lg-4">
				<img class="img-circle" src="../images/perfilMarina.jpg"
					alt="Foto Marina">
				<h2 class="img-circle-sub">Marina</h2>
				<p align="justify">Marina � bi�loga e especialista em animais silvestres. Sempre
					amou c�es e, ap�s passar alguns anos trabalhando em escrit�rio,
					percebeu que n�o era isso que queria. Resolveu fazer o curso de
					passeadores da c�o ativo e passou a se dedicar aos c�es e a uma
					melhor intera��o deles com seus donos.</p>
			</div>
		</div>
		<!-- /.row -->
		<div hidden="true" class="row">
			<div class="col-lg-4">
				<img class="img-circle" src="../images/perfilDebora.png"
					alt="Foto Nat�lia">
				<h2 class="img-circle-sub">Nat�lia</h2>
				<p align="justify">D�bora � bi�loga e educadora ambiental. Depois de passar
					alguns anos dando aulas de biologia, resolveu deixar tudo para
					trabalhar com c�es, j� que sempre teve paix�o por eles. Fez o curso
					de Passeador de c�es da C�o Ativo e hoje oferece passeios
					orientados e outros cuidados para os cachorros da Matilha da
					Cidade.</p>
			</div>
			<div class="col-lg-4">
				<img class="img-circle" src="../images/perfilDebora.png"
					alt="Foto Zilda">
				<h2 class="img-circle-sub">Zilda</h2>
				<p align="justify">D�bora � bi�loga e educadora ambiental. Depois de passar
					alguns anos dando aulas de biologia, resolveu deixar tudo para
					trabalhar com c�es, j� que sempre teve paix�o por eles. Fez o curso
					de Passeador de c�es da C�o Ativo e hoje oferece passeios
					orientados e outros cuidados para os cachorros da Matilha da
					Cidade.</p>
			</div>
			<div class="col-lg-4">
				<img class="img-circle" src="../images/perfilDebora.png"
					alt="Foto Barbara">
				<h2 class="img-circle-sub">Barbara</h2>
				<p align="justify">D�bora � bi�loga e educadora ambiental. Depois de passar
					alguns anos dando aulas de biologia, resolveu deixar tudo para
					trabalhar com c�es, j� que sempre teve paix�o por eles. Fez o curso
					de Passeador de c�es da C�o Ativo e hoje oferece passeios
					orientados e outros cuidados para os cachorros da Matilha da
					Cidade.</p>
			</div>
		</div>
		<!-- /.row -->
	</div>

	<!-- Site footer -->
	<jsp:include page="modules/footer.jsp">
		<jsp:param name="callerName" value="about" />
	</jsp:include>
	<!-- /footer -->

	<!-- Bootstrap core JavaScript
	    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>

</body>
</html>