<%@ page import="user.*"%>

<div class="navbar-wrapper">
	<div class="container">
		<!-- Fixed navbar -->
		<nav id="navbarContainer" class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<!--  Esse botao e oque aparece quando abre no mobile -->
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<% if(request.getParameter("callerName").equals("index")) { %>
					<a class="navbar-brand" href="index.jsp"><img id="MainBarLogo"
						src="images/logo.png" /></a>
					<%} else { %>
					<a class="navbar-brand" href="../index.jsp"><img
						id="MainBarLogo" src="../images/logo.png" /></a>
					<%} %>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul id="navBarItens" class="nav navbar-nav">
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown" role="button" aria-expanded="false">
								<% if(request.getParameter("callerName").equals("index")) { %>
								<img class="buttonImg" src="images/buttonImg.png">Sobre
								<span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="pages/about.jsp">Quem Somos</a></li>
									<li><a href="pages/mission.jsp">Miss�o</a></li>
								</ul>
						</li>
						<li><a href="pages/services.jsp"><img class="buttonImg" src="images/buttonImg.png">Servi�os</a></li>
						<li><a href="pages/contact.jsp"><img class="buttonImg" src="images/buttonImg.png">Contatos</a></li>

						<%} else { %>
							<img class="buttonImg" src="../images/buttonImg.png">Sobre
							<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="about.jsp">Quem Somos</a></li>
								<li><a href="mission.jsp">Miss�o</a></li>
							</ul>
						</li>
						<li><a href="services.jsp"><img class="buttonImg" src="../images/buttonImg.png">Servi�os</a></li>
						<li><a href="contact.jsp"><img class="buttonImg" src="../images/buttonImg.png">Contatos</a></li>

						<%} %>

					</ul>
					<ul id="navBarLogos" class="nav navbar-nav navbar-right">
						<% if(request.getParameter("callerName").equals("index")) { %>
						<li><a class="linkOtherLogos"
							href="https://www.facebook.com/MatilhaDaCidade" target="_blank"><img
								class="imageOtherLogos" src="images/facebook.png" width="30px"
								height="30px"></a></li>
						<li><a class="linkOtherLogos"
							href="http://instagram.com/matilha.da.cidade/" target="_blank"><img
								class="imageOtherLogos" src="images/Instagram.png" width="30px"
								height="30px"></a></li>
						<%} else { %>
						<li><a class="linkOtherLogos"
							href="https://www.facebook.com/MatilhaDaCidade" target="_blank"><img
								class="imageOtherLogos" src="../images/facebook.png"
								width="30px" height="30px"></a></li>
						<li><a class="linkOtherLogos"
							href="http://instagram.com/matilha.da.cidade/" target="_blank"><img
								class="imageOtherLogos" src="../images/Instagram.png"
								width="30px" height="30px"></a></li>
						<% } %>
					</ul>
						<% if(request.getSession().getAttribute("loggedUser") == null && request.getParameter("callerName").equals("blog")){%>
						<form method="POST" id="loginForm" class="navbar-form navbar-right"
							role="search" action="modules/login.jsp"> 
							<div class="form-group">
								<input class="loginInput" type="text" class="form-control" name="username"
									placeholder="Username" width="150px" />
							</div>
							<div class="form-group">
								<input class="loginInput" type="password" class="form-control" name="password"
									placeholder="Senha" width="150px" />
							</div>
							<button type="submit" class="btn btn-default">Entrar</button>
						</form>
						<% } %>
				</div>
			</div>
		</nav>
		<br> <br>
	</div>
</div>