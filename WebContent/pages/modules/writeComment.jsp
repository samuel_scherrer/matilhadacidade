<head>
<!-- Custom styles for this template -->
<link href="../css/custom/writeCommentStyle.css" rel="stylesheet">
</head>

<%String postID = request.getParameter("post_id"); %>
			
<div class="jumbotron writecomments" id="createSpan-<%=postID%>"
	style="display: <%=request.getParameter("default_display")%>">
	<form method="POST" name="newComment_form" action="modules/new_comment_handler.jsp">
		<div class="form-group">
			<label for="exampleInputPassword1">Autor</label> <input
				name="comment_author" type="text" class="form-control"
				placeholder="Autor">
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">Comentário</label>
			<textarea class="form-control" rows="5" name="comment_text"
				placeholder="Digite o seu comentário..."></textarea>
		</div>
		<input type="hidden" name="postID"  value="<%=postID%>" />
		<button type="submit" class="btn btn-default">Comentar</button>
	</form>
</div>