<head>
<!-- Custom styles for this template -->
<link href="../css/custom/postStyle.css" rel="stylesheet">

<script>

function showComments(postID){
	document.getElementById("postid-" + postID).style.display = 'block';
	document.getElementById("postCommentShowBtn-" + postID).style.display = 'none';
	document.getElementById("postCommentHideBtn-" + postID).style.display = 'block';
}

function hideComments(postID){
	document.getElementById("postid-" + postID).style.display = 'none';
	document.getElementById("postCommentShowBtn-" + postID).style.display = 'block';
	document.getElementById("postCommentHideBtn-" + postID).style.display = 'none';
}

function showCreateSpan(postID){
	document.getElementById("createSpan-" + postID).style.display = 'block';
	document.getElementById("createBtn-" + postID).style.display = 'none';	
}
</script>

</head>

<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ page import="org.gjt.mm.mysql.*"%>

<% String postID = request.getParameter("post_id"); %>

<!-- Main component for a primary marketing message or call to action -->
<div href="#<%=request.getParameter("post_id")%>"
	class="jumbotron post-item">
	
			<!-- Alert sobre envio do comentário -->
			<%if (session.getAttribute("messageStatus-"+ postID) == null) {
			} else {%>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				Erro ao realizar o comentário:
				<%=session.getAttribute("errorString-"+ postID)%>
			</div>
			<%session.setAttribute("messageStatus-"+ postID, null);
			}%>
			<!-- /Alert -->
	
	<h4 id="post-item-header"><%=request.getParameter("post_title")%></h4>
	<p id="post-item-text"><%=request.getParameter("post_text")%></p>
	<p id="post-item-author">
		MatilhaDaCidade@<%=request.getParameter("post_author")%>
		em
		<%=request.getParameter("post_date")%></p>
	<a onclick=""><%=request.getParameter("post_likes")%> likes</a> <br>

	<%if(request.getParameter("post_hasComments").equals("true")){ %>
	<a id="postCommentShowBtn-<%=request.getParameter("post_id")%>"
		onclick="showComments('<%=postID%>')">Ver
		comentários</a> <a
		id="postCommentHideBtn-<%=postID%>"
		onclick="hideComments('<%=postID%>')"
		style="display: none">Ocultar comentários</a>
</div>
<%} else { %>
<a id="createBtn-<%=postID%>"
	onclick="showCreateSpan('<%=postID%>')">Comentar</a>
</div>
	<!-- Write Comment -->
	<jsp:include page="writeComment.jsp">
		<jsp:param name="post_id" value="<%=postID%>" />
		<jsp:param name="default_display" value="none" />
	</jsp:include>
	<!-- /.Write Comment -->
<% }%>

