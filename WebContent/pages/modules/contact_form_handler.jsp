<%@ page import="java.io.*,java.util.*,javax.mail.*"%>
<%@ page import="javax.mail.internet.*,javax.activation.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@ page import="email.components.EmailHandler" %>

<%
		String result;
		// Recipient's email ID needs to be mentioned.
		String to = "matilhadacidade@gmail.com";

		// Sender's email ID needs to be mentioned
		String from = "matilhadacidade@gmail.com";

		// Assuming you are sending email from localhost
		String host = "smtp.gmail.com";

		// Account pass
		String pass = "marinalinda";
		
		//Fields from form
		String senderEmail = request.getParameter("sender_email");
		String senderName = request.getParameter("sender_name");
		String emailBody = request.getParameter("email_body");
		String robotRand1 = request.getParameter("robotRand1");
		String robotRand2 = request.getParameter("robotRand2");
		String robotResult = request.getParameter("robot_check");
		
		EmailHandler emailHelper = new EmailHandler(senderEmail, senderName, emailBody, robotResult, robotRand1, robotRand2);
		boolean fieldsHasError = emailHelper.checkInputConsistence();
		String erroMsgStack = emailHelper.getErrorMsg();
		
		
		//If any erro was catch return with the erro message on the attribute
		if(!fieldsHasError){
			// Get system properties object
			Properties properties = new Properties();
	
			// Setup mail server
			properties.put("mail.smtp.host", host);
			properties.put("mail.transport.protocol", "smtp");
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.ssl.enable", "true");
			properties.put("mail.user", from);
			properties.put("mail.password", pass);
			properties.put("mail.port", "465");
			properties.put("mail.smtp.timeout", "25000");
	
			// Get the default Session object.
			Session mailSession = Session.getDefaultInstance(properties, null);
	
			try {
				// Create a default MimeMessage object.
				MimeMessage message = new MimeMessage(mailSession);
				// Set From: header field of the header.
				message.setFrom(new InternetAddress(from));
				// Set To: header field of the header.
				message.addRecipient(Message.RecipientType.TO,
						new InternetAddress(to));
				// Set Subject: header field
				message.setSubject(request.getParameter("email_subject"));
				// Now set the actual message
				String strMsg = "Email enviado por: " + senderName + "\nEmail: " + 
				senderEmail + "\nMensagem:\n" + emailBody;
				message.setText(strMsg);
				// Send message
				Transport transport = mailSession.getTransport("smtp");
				transport.connect(host, from, pass);
				transport.sendMessage(message, message.getAllRecipients());
				transport.close();
				session.setAttribute("messageStatus", "SENT");
			} catch (MessagingException mex) {
				mex.printStackTrace();
				session.setAttribute("messageStatus", "ERROR");
				session.setAttribute("errorString", mex.getMessage());
			}
		}
		else{
			session.setAttribute("messageStatus", "ERROR");
			session.setAttribute("errorString", erroMsgStack);
		}
		
		response.sendRedirect("../contact.jsp");
%>