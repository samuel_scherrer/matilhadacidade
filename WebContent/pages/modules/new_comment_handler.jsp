<%@ page import="blog.comment.Comment" %>
<%@ page import="blog.comment.CommentHandler" %>
<%@ page import="dao.comment.CommentDAO" %>
<%@ page import="java.io.*,java.util.Date"%>
<%@ page import="java.text.*, java.sql.*"%>


<%
	//Fields from form
	String commentSender = request.getParameter("comment_author");
	String commentText = request.getParameter("comment_text");
	String postID = request.getParameter("postID");
	
	CommentHandler commentHandler = new CommentHandler(commentSender, commentText);
	boolean hasErrors = commentHandler.checkInputConsistence();
	String erroMsgStack = commentHandler.getErrorMsg();
	
	//If any erro was catch return with the erro message on the attribute
	if(!hasErrors){
		//Pegamos a data atual
		Date date = new Date();
		
		//Definimos o formato como a data ser� armazenada
		SimpleDateFormat dateFormated = new SimpleDateFormat("dd/MM/yyyy");
		
		CommentDAO commentDAO = new CommentDAO();
		Comment comment = new Comment(commentText, dateFormated.format(date), commentSender);
		commentDAO.insertComment(comment, postID);
	}
	else{
		session.setAttribute("messageStatus-"+postID, "ERROR");
		session.setAttribute("errorString-"+postID, erroMsgStack);
	}
	
	response.sendRedirect("../blog.jsp");

%>