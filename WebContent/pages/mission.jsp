<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Matilha da Cidade</title>

<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="../css/custom/navBar.css" rel="stylesheet">
<link href="../css/custom/missionPage.css" rel="stylesheet">
<link href="../css/custom/generalStyle.css" rel="stylesheet">
<link href="../css/custom/footer.css" rel="stylesheet">
<link href="../css/custom/carousel_wrap.css" rel="stylesheet">


</head>

<body id="body">

	<!-- NavBar -->
	<jsp:include page="modules/topNavBar.jsp">
		<jsp:param name="callerName" value="mission" />
	</jsp:include>
	<!-- /.NavBar -->

	<div id="content-container" class="container">

		<div id="headerImage" class="jumbotron">
			<h1 id="headerText">Nossa Miss�o</h1>
		</div>

		<!-- Main component for a primary marketing message or call to action -->
		<div class="jumbotron">
			<p align="justify">A miss�o da Matilha da Cidade � proporcionar aos c�es momentos
				em que eles possam desenvolver seus instintos, sendo n�mades,
				caminhando diariamente, farejando e seguindo um l�der de forma
				equilibrada, mantendo assim sua sa�de e bem estar.</p>
		</div>
	</div>
	<!-- /container -->

	<!-- Carousel_wrap -->
	<jsp:include page="modules/carousel_wrap.jsp">
		<jsp:param name="callerName" value="mission" />
	</jsp:include>
	<!-- /.Carousel_wrap -->

	<!-- Site footer -->
	<jsp:include page="modules/footer.jsp">
		<jsp:param name="callerName" value="mission" />
	</jsp:include>
	<!-- /footer -->

	<!-- Bootstrap core JavaScript
	    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>

</body>
</html>