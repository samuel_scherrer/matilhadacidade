package email.components;

import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;

import values.ConstString;

public class EmailHandler {
	private String senderEmail;
	private String senderName;
	private String message;
	private String antiRobotTest;
	private String antiRobotRand1;
	private String antiRobotRand2;
	private String errorStackMsg;
	
	public EmailHandler(String toEmail, String name, String msg, String testSum, String testRand1, String testRand2){
		senderEmail = toEmail;
		senderName = name;
		message = msg;
		antiRobotTest = testSum;
		antiRobotRand1 = testRand1;
		antiRobotRand2 = testRand2;
		
		errorStackMsg = "";
	}
	
	public boolean checkInputConsistence(){
		boolean hasError = false;
		
		if(senderEmail == null || senderEmail.equals("")){
			hasError = true;
			errorStackMsg = ConstString.error_email_missing;
		}
		if(senderName == null || senderName.equals("")){
			hasError = true;
			errorStackMsg = errorStackMsg + ConstString.error_name_missing;
		}
		if(message == null || message.equals("")){
			hasError = true;
			errorStackMsg = errorStackMsg + ConstString.error_message_missing;
		}
		if(antiRobotTest == null || antiRobotTest.equals("")){
			hasError = true;
			errorStackMsg = errorStackMsg + ConstString.error_robot_check_missing;
		}
		else{
			int sum = Integer.parseInt(antiRobotTest);
			if(antiRobotRand1 != null && antiRobotRand2 != null){
				int rand1 = Integer.parseInt(antiRobotRand1);
				int rand2 = Integer.parseInt(antiRobotRand2);
				
				if(sum != (rand1 + rand2)){
					hasError = true;
					errorStackMsg = errorStackMsg + ConstString.error_robot_sum_not_match;
				}
			}
			else{
				hasError = true;
				errorStackMsg = errorStackMsg + ConstString.error_generated_params;
			}
		}
		return hasError;
	}
	
	public String getErrorMsg(){
		return errorStackMsg;
	}
}
