package values;

public class ConstString {
	public static final String error_email_missing = "Campo email deve ser preenchido" + "<br>";
	public static final String error_name_missing = "Campo nome deve ser preenchido" + "<br>";
	public static final String error_message_missing = "Campo mensagem deve ser preenchido" + "<br>";
	public static final String error_robot_check_missing = "Campo de teste contra rob� deve ser preenchido" + "<br>";
	public static final String error_robot_sum_not_match = "A soma de verifica��o n�o confere" + "<br>";
	public static final String error_generated_params = "Erro nos parametros gerados" + "<br>";
	public static final String error_comment_author_missing = "Campo autor deve ser preenchido" + "<br>";
	public static final String error_comment_text_missing = "Campo com o coment�rio deve ser preenchido" + "<br>";
}
