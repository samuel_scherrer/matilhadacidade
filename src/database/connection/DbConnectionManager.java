package database.connection;

import java.sql.*;

public class DbConnectionManager {
	public static final String url = "jdbc:mysql://localhost:3306/";
	public static final String dbName = "matilha";
	public static final String driver = "org.gjt.mm.mysql.Driver";
	public static final String userName = "matilha";
	public static final String password = "marinalinda";
	private Connection conn;
	private boolean isConnected;
	private String connStatus;
	
	public DbConnectionManager(){
		isConnected = false;
		try{
			Class.forName(driver).newInstance();
			getConnection();
		}
		catch(Exception e){
			
		}
	}
	
	public Statement getStatement(){		
		try {
			if(!isConnected)
				getConnection();
			Statement statement = conn.createStatement();
			return statement;
		}
		catch(Exception e){
			return null;
		}
	}
	
	private void getConnection(){
		try{
			conn = DriverManager.getConnection(url+dbName, userName, password);
			if(conn != null){
				//logar que a conexao foi feita
				connStatus = "Conex�o feita com sucesso";
				isConnected = true;
			}
			else {
				//logar que a conexao nao foi feita
				connStatus = "Conex�o n�o realizada";
				isConnected = false;
			}
		}
		catch(Exception e){

		}
	}
	
	public boolean closeConnection(){
		try{
			conn.close();
			isConnected = false;
			return true;
		}
		catch(Exception e){
			return false;
		}
	}
	
	public String getStatus(){
		return connStatus;
	}
	
	public boolean hasConnection(){
		return isConnected;
	}
	
}
