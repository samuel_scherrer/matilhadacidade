package blog.post;

import java.util.List;

import blog.comment.Comment;

public class Post {
	private String message;
	private String author;
	private String date;
	protected String title;
	private int likes;
	private List<Comment> comments;
	protected int id;
	
	public Post(String message, String author, String date, String title, int likes, List<Comment> comments, int id) {
		this.message = message;
		this.author = author;
		this.date = date;
		this.title = title;
		this.likes = likes;
		this.comments = comments;
		this.id = id;
	}
	
	public Post(String message, String author, String date, String title) {
		this.message = message;
		this.author = author;
		this.date = date;
		this.title = title;
		this.likes = 0;
	}
	
	public int getID(){
		return id;
	}
	
	public String getMessage() {
		return message;
	}

	public String getAuthor() {
		return author;
	}

	public String getDate() {
		return date;
	}

	public String getTitle() {
		return title;
	}

	public int getLikes() {
		return likes;
	}

	public List<Comment> getComments() {
		return comments;
	}
	
}
