package blog.post;

import java.util.List;

import blog.comment.Comment;

public class PostFragment extends Post {
	private String messageFragment;
	private final int MAXSPACEPOS = 150;
	
	public PostFragment(String message, String author, String date, String title, int id){
		super(message, author, date, title, 0, null, id);
		
		//Verificamos o tamanho da message e cortamos ela caso seja muito grande
		int spacePos = 150;
		if (message.length() >= MAXSPACEPOS) {
			//Procuramos por um " "
			for (int i = 140; i < message.length(); i++) {
				char charAt = message.charAt(i);
				if (charAt == ' ') {
					spacePos = i;
					break;
				}
			}
			//Cortamos a string na posicao indica pelo spacePos
			messageFragment = message.substring(0, spacePos);
			
			//Inserimos ap�s o corte um link para a pagina de postes
			messageFragment = messageFragment + "...<a href=\"pages/blog.jsp#" + id + "\">Ver mais</a>";
		}
		//Se � menor que 150 caracteres ent�o n�o precisa cortar a string
		else {
			messageFragment = message;
			
			//Inserimos ap�s o corte um link para a pagina de postes
			messageFragment = messageFragment + " <a href=\"pages/blog.jsp#" + id + "\">Ver mais</a>";
		}
	}
	
	public String getMessage(){
		return messageFragment;
	}
	
	public int getID(){
		return id;
	}
	
	public String getTitle(){
		return title;
	}
}
