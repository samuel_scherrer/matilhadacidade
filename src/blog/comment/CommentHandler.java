package blog.comment;

import values.ConstString;

public class CommentHandler {
	private String commentAuthor;
	private String commentText;
	private String errorStackMsg;
	
	public CommentHandler(String author, String text){
		commentAuthor = author;
		commentText = text;
		
		errorStackMsg = "";
	}
	
	public boolean checkInputConsistence(){
		boolean hasError = false;
		
		if(commentAuthor == null || commentAuthor.equals("")){
			hasError = true;
			errorStackMsg = ConstString.error_comment_author_missing;
		}
		if(commentText == null || commentText.equals("")){
			hasError = true;
			errorStackMsg = errorStackMsg + ConstString.error_comment_text_missing;
		}
		
		return hasError;
	}
	
	public String getErrorMsg(){
		return errorStackMsg;
	}
}
