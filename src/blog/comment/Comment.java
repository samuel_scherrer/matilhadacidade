package blog.comment;

public class Comment {
	private String message;
	private String data;
	private int id;
	private String author;
	
	public Comment(String message, String data, int id, String author) {
		this.message = message;
		this.data = data;
		this.id = id;
		this.author = author;
	}
	
	public Comment(String message, String data, String author) {
		this.message = message;
		this.data = data;
		this.author = author;
	}

	public String getMessage() {
		return message;
	}

	public String getData() {
		return data;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public int getID(){
		return id;
	}
}
