package user;
import java.text.SimpleDateFormat;
import java.util.Date;


public class User {
	private final String userName;
	private final Date loginDate;
	private final boolean admin;
	
	public User(String userName, boolean isAdmin) {
		this.userName = userName;
		loginDate = new Date();
		admin = isAdmin;
	}
	
	public String getUserName(){
		return userName;
	}
	
	public String getLoginDate() {
		SimpleDateFormat dateFormated = new SimpleDateFormat("dd/MM/yyyy");
		return dateFormated.format(loginDate);
	}
	
	public boolean isAdmin(){
		return admin;
	}
}
