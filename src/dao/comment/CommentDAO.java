package dao.comment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import database.connection.DbConnectionManager;
import blog.comment.Comment;
import blog.post.Post;

public class CommentDAO {
	private DbConnectionManager connManager;
	private ResultSet res;
	private static final String TABLE = "comment";
	
	public CommentDAO(){
		connManager = new DbConnectionManager();
	}
	
	public boolean insertComment(Comment comment, String postID){
		boolean result = false;
		
		// Como queremos inserir um novo post criamos um query de insert into
		String sql = "INSERT INTO " + TABLE
				+ " (author, text, date, id_post) VALUES ('"
				+ comment.getAuthor() + "', '" + comment.getMessage() + "', '"
				+ comment.getData() + "', '" + postID + "')";
		try {
			Statement statement = connManager.getStatement();
			int updatesRes = statement.executeUpdate(sql);
			if(updatesRes > 0)
				result = true;
			statement.close();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			connManager.closeConnection();
		}
		
		return result;
	} 
	
	public List<Comment> getCommentsByPostID(int id) {
		List<Comment> comments = new ArrayList<Comment>();
		
		String sql = "SELECT * FROM "  + TABLE + " WHERE id_post = " + id + " ORDER BY date DESC";
		
		try {
			Statement statement = connManager.getStatement();
			res = statement.executeQuery(sql);
			
			while (res.next()) {
				//Recuperamos as informações
				int comment_id = res.getInt("id");
				String text = res.getString("text");
				String author = res.getString("author");
				String date = res.getString("date");

				Comment comment = new Comment(text, date, comment_id, author);
				comments.add(comment);
			}
			statement.close();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			connManager.closeConnection();
		}
		return comments;
	}
}
