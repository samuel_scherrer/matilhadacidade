package dao.post;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import blog.comment.Comment;
import blog.post.Post;
import dao.comment.CommentDAO;
import database.connection.DbConnectionManager;

public class PostDAO {
	private DbConnectionManager connManager;
	private ResultSet res;
	private static final String TABLE = "post";
	private CommentDAO commentDAO;
	
	public PostDAO(){
		connManager = new DbConnectionManager();
		commentDAO = new CommentDAO();
	}
	
	public List<Post> getAll(){
		List<Post> postList = new ArrayList<Post>();
		
		String sql = "SELECT * FROM "  + TABLE + " ORDER BY id DESC";
		
		try {
			Statement statement = connManager.getStatement();
			res = statement.executeQuery(sql);
			
			while (res.next()) {
				//Recuperamos as informações
				int id = res.getInt("id");
				
				//Temos que buscar a lista de comentarios dado o ID do post
				List<Comment> comments = commentDAO.getCommentsByPostID(id);
				String title = res.getString("title");
				String text = res.getString("text");
				String author = res.getString("author");
				int likes = res.getInt("likes");
				String date = res.getString("date");
				
				Post post = new Post(text, author, date, title, likes, comments, id);
				postList.add(post);
			}
			statement.close();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			connManager.closeConnection();
		}
		
		return postList;
	}
	
	public boolean insertPost(Post post){
		boolean result = false;
		
		// Como queremos inserir um novo post criamos um query de insert into
		String sql = "INSERT INTO " + TABLE
				+ " (author, title, text, date, likes) VALUES ('"
				+ post.getAuthor() + "', '" + post.getTitle() + "', '"
				+ post.getMessage() + "', '" + post.getDate() + "', '"
				+ post.getLikes() + "')";
		try {
			Statement statement = connManager.getStatement();
			int updatesRes = statement.executeUpdate(sql);
			if(updatesRes > 0)
				result = true;
			statement.close();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			connManager.closeConnection();
		}
		
		return result;
	}
}
