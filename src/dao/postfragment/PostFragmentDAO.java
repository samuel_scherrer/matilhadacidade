package dao.postfragment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import database.connection.DbConnectionManager;
import blog.post.PostFragment;

public class PostFragmentDAO {
	private DbConnectionManager connManager;
	private ResultSet res;
	private static final String TABLE = "post";
	
	public PostFragmentDAO(){
		connManager = new DbConnectionManager();
	}
	
	public List<PostFragment> getAll(){
		List<PostFragment> postFragList = new ArrayList<PostFragment>();
		
		String sql = "SELECT * FROM "  + TABLE + " ORDER BY id DESC";

		try {
			Statement statement = connManager.getStatement();
			res = statement.executeQuery(sql);
			
			while (res.next()) {
				//Recuperamos as informações
				int id = res.getInt("id");
				String title = res.getString("title");
				String text = res.getString("text");
				String author = res.getString("author");
				String date = res.getString("date");
				
				PostFragment postFrag = new PostFragment(text, author, date, title, id);
				postFragList.add(postFrag);
			}
			statement.close();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			connManager.closeConnection();
		}
		
		return postFragList;
	}
	
	public String getConnStatus() {
		return connManager.getStatus();
	}
}
